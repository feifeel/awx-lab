#! /bin/bash
# Author: kwan.chen
# check conntrack data

# greater than critial
critial_percentage=100

# greater than warning
warning_percentage=80

# less than safe
safe_percentage=80

echo "Conntrack Check"
# use -n option to only return the value
conntrack_current=$(sysctl -n net.netfilter.nf_conntrack_count)
conntrack_max=$(sysctl -n net.netfilter.nf_conntrack_max)

# check whether the conntrack value is valid
regular_expression_number_patter='^[0-9]+$'
if ! [[ ($conntrack_current =~ $regular_expression_number_patter) || ($conntrack_max =~ $regular_expression_number_patter) ]] ; then
    echo "Command 'sysctl -n net.netfilter.nf_conntrack_count' CANNOT return an integer number."
    echo "OR"
    echo "Command 'sysctl -n net.netfilter.nf_conntrack_max' CANNOT return an integer number."
    echo "Conntrack Module might NOT be installed on the machine."
    echo "Installation Steps (CentOS 7):"
    echo "sudo yum install conntrack-tools libnetfilter_conntrack"
    echo "systemctl enable conntrackd.service"
    echo "systemctl start conntrackd.service"
    exit 1
fi

echo "Current Conntrack: ${conntrack_current}"
echo "Maximum Conntrack: ${conntrack_max}"

# use python to do integer calculation is simpler
conntrack_percentage_in_used=$(python -c "percentage = ${conntrack_current} * 100 / ${conntrack_max}; print(percentage)")
echo "Percentage (Integer): ${conntrack_percentage_in_used}%"

if [ $conntrack_percentage_in_used -ge $critial_percentage ]; then
    echo "Conntrack Check Result: Critial"
    exit 2
elif [ $conntrack_percentage_in_used -ge $warning_percentage ]; then
    echo "Conntrack Check Result: Warning"
    exit 1
elif [ $conntrack_percentage_in_used -lt $safe_percentage ]; then
    echo "Conntrack Check Result: OK"
    exit 0
# in case miss considering some scenarios
else
    echo "The checking might hit a bug."
    exit 1
fi