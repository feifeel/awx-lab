#!/bin/bash
#---------------------------------------------------------------------------
#title           :check_metricbeat.sh
#description     :check metricbeat service state.
#author          :Robin Guo
#date            :20190123
#version         :1.0


# Define metricbeat state value
metricbeat_state_up=0
metricbeat_state_down=2



# Check the state of  metricbeat's service
#metricbeat_rpm_num=$(rpm -qa|grep 'metricbeat'|grep -v 'metricbeat_check'|grep -v 'grep'|wc -l 2>/dev/null)
metricbeat_service_num=$(ps -ef|grep 'metricbeat'|grep -v 'metricbeat_check'|grep -v 'grep'|wc -l 2>/dev/null)
cenos_version=$(egrep -o '([0-9].)+' /etc/system-release|awk -F . '{print $1}' 2>/dev/null)



# Get the state of metricbeat service
  if [[ $metricbeat_service_num -ge 1 ]];then
      metricbeat_state="up"
   else
      metricbeat_state="down"
  fi





# main program

case $cenos_version in
  5|6)

    #  metricbeat state is ok
    if  [[ $metricbeat_state == "up" ]];then
      echo "Metricbeat Service state: $metricbeat_state"
      /etc/init.d/metricbeat status
      echo "Server OS: CentOS ${cenos_version}"
      exit $metricbeat_state_up

    # if metricbeat is down, then try to start it , after start and failed again, will trigger a consul alerts with metricbeat is down.
    elif  [[ $metricbeat_state == "down" ]];then
      /etc/init.d/metricbeat start >/dev/null 2>&1
      sleep 5
      wait
      try_run=$(/etc/init.d/metricbeat status |egrep -io 'run')

      if [[ $try_run == 'run' ]];then
        echo "Metricbeat Service state: up"
        /etc/init.d/metricbeat status
        echo "Server OS: CentOS ${cenos_version}"
        exit $metricbeat_state_up
      else
        echo "Metricbeat Service state: $metricbeat_state"
        /etc/init.d/metricbeat status
        echo "Server OS: CentOS ${cenos_version}"
        exit $metricbeat_state_down
      fi

    fi

  ;;


  7)
    #  metricbeat state is ok
    if  [[ $metricbeat_state == "up" ]];then
      echo "Metricbeat Service state: $metricbeat_state"
      systemctl status metricbeat|grep -i 'Active' | awk -F: '{$1=""; print}'|sed 's/^[ \t]*//g'
      echo "Server OS: CentOS ${cenos_version}"
      exit $metricbeat_state_up

    # if metricbeat is down, then try to start it , after start and failed again, will trigger a consul alerts with metricbeat is down.
    elif  [[ $metricbeat_state == "down" ]];then
      systemctl start metricbeat >/dev/null 2>&1
      sleep 5
      wait
      try_run=$(systemctl status metricbeat |grep -i 'Active' | awk -F: '{$1=""; print}'|sed 's/^[ \t]*//g'|egrep -io 'running')
      if [[ $try_run == "running" ]];then
        echo "Metricbeat Service state: up"
        systemctl status metricbeat|grep -i 'Active' | awk -F: '{$1=""; print}'|sed 's/^[ \t]*//g'
        echo "Server OS: CentOS ${cenos_version}"
        exit $metricbeat_state_up
      else
        echo "Metricbeat Service state: $metricbeat_state"
        systemctl status metricbeat|grep -i 'Active' | awk -F: '{$1=""; print}'|sed 's/^[ \t]*//g'
        echo "Server OS: CentOS ${cenos_version}"
        exit $metricbeat_state_down
      fi

    fi

  ;;

esac
