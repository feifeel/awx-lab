#!/usr/bin/env python2

import sys
import os
import subprocess
import time
import json
from datetime import datetime

is_test_mode_on = True  # test switch

# consul check exit codes
exit_code_passing = 0
exit_code_warning = 1
exit_code_critical = 2

# Logging
check_log = 'Puppet Service Check Log'


# use this function to replace the default print function
def output_to_log(message):
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print('{time_stamp} {new_message}'.format(time_stamp=current_time, new_message=message))
    global check_log
    check_log = '{previous_log}\n{time_stamp} {new_message}'.format(
        previous_log=check_log,
        time_stamp=current_time,
        new_message=message)


def brocast_message(message):
    bash_command = 'printf "{message}" | wall'.format(message=message)
    subprocess.call(
        bash_command,
        shell=True)


def get_short_hostname():
    bash_command = 'hostname -s'
    return_result = os.popen(bash_command).read().rstrip()
    return return_result


def is_service_running(service_name):
    bash_command = 'service {service_name} status'.format(service_name=service_name)
    return_result = os.popen(bash_command).read().rstrip()
    output_to_log(return_result)
    if 'is running...' in return_result or 'Active: active (running)' in return_result:
        return True
    else:
        return False


def stop_service(service_name):
    output_to_log('===== Try to Stop Puppet Service')
    bash_command = 'service {service_name} stop'.format(service_name=service_name)
    return_result = os.popen(bash_command).read().rstrip()
    output_to_log(return_result)


def is_puppet_on_maintenance(hostname):
    output_to_log('===== Check Puppet Maintenance Status on Consul KV Store')
    bash_command = '/usr/local/consul/bin/consul kv get maintenance/puppet/{hostname}'.format(hostname=hostname)
    return_result = os.popen(bash_command).read().rstrip()
    try:
        json_object = json.loads(return_result)
    except ValueError, e:  # key NOT found in Consul KV Store
        output_to_log('Puppet is NOT on maintenance.')
        return False
    if json_object['on_maintenance'] and json_object['admin_name']:
        output_to_log('Puppet is set to be on maintenance on Consul KV Store by {admin}'.format(admin=json_object['admin_name']))
        return True
    else:  # key found in Consul KV Store, but value not as expected
        output_to_log('Puppet Service Maintenance Key-Value Pair is NOT set correctly.')
        return False


puppet_service_name = 'puppet'
hostname = get_short_hostname()
output_to_log('===== Puppet Service Check - {host} ====='.format(host=hostname))
# on maintenance, puppet is stopped
if is_puppet_on_maintenance(hostname):
    if not is_service_running(puppet_service_name):
        output_to_log('Puppet Service is stopped on purpose.')
        sys.exit(exit_code_warning)
    else:
        # on maintenance, puppet is still running, perform auto stop
        output_to_log('Puppet Service is NOT stopped. Try to stop Puppet Service automatically.')
        stop_service(puppet_service_name)
        time.sleep(10)  # wait for 10 secs before a verify check
        if is_service_running(puppet_service_name):
            output_to_log('Puppet Service is still running, fail to stop the service.')
            sys.exit(exit_code_warning)
        else:
            output_to_log('Puppet Service is stopped on purpose for maintenance.')
            # inform all logined admins
            brocast_message(check_log)
            sys.exit(exit_code_critical)
else:
    if is_service_running(puppet_service_name):
        output_to_log('Puppet Service is running as expected.')
        sys.exit(exit_code_passing)
    else:
        output_to_log('Puppet Service is unexpectedly stopped. Let admin to manually restart. \nIf service is stopped on purpose, please add maintenance key on Consul KV Store.')
        sys.exit(exit_code_critical)

'''
# Puppet Check Explain
This Puppet Check works with the Consul KV Store. In order to take advantage of the convinience it provides, you will nedd to follow the convention.
If you want to stop Puppet Service for maintenance or testing, you can do that by adding a Consul key-value pair:
key: maintenance/puppet/server_short_hostname
value: (in JSON format)

{
    "on_maintenance": true,
    "admin_name": "your_name"
}

If you want to remove the maintenance mode, just delete the key, maintenance/puppet/server_short_hostname. 
It's your responsibility to start the service. The Puppet check will NOT do it for you.

Once you make the Puppet service for the server to be on maintenance on the KV store, the Puppet Check will automatically stop the service for you.

In case Puppet service being started unexpectedly during your maintenance or testing. This script does NOT start Puppet Service in any case.
'''
